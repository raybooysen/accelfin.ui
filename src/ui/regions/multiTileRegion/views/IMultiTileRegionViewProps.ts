import MultiTileRegionModel from '../model/multiTileRegionModel';
import {IViewBaseProps} from '../../../viewBase';

interface IMultiTileRegionViewProps extends IViewBaseProps<MultiTileRegionModel> {
    className?: string;
}

export default IMultiTileRegionViewProps;
