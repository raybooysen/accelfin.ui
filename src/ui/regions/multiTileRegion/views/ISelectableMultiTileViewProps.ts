import MultiTileRegionModel from '../model/multiTileRegionModel';
import {IViewBaseProps} from '../../../viewBase';

interface ISelectableMultiTileViewProps extends IViewBaseProps<MultiTileRegionModel> {
    className?: string;
}
export default ISelectableMultiTileViewProps;