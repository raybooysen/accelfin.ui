export default class MultiTileRegionEventConst {
    static get selectedTileChanged() : string {
        return 'selectedTileChanged';
    }
}
