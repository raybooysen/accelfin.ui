import SingleItemRegionsModel from '../model/singleItemRegionsModel';
import {IViewBaseProps} from '../../../viewBase';

interface ISingleItemRegionViewProps extends IViewBaseProps<SingleItemRegionsModel> {
    className?: string;
}

export default ISingleItemRegionViewProps;