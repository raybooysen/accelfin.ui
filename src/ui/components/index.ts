export { default as ComponentRegistryModel } from './componentRegistryModel';
export { default as ComponentFactoryBase } from './componentFactoryBase';
export { default as ComponentMetadata } from './componentMetadata';
export { default as FactoryEntry } from './factoryEntry';
export { componentFactory, getComponentFactoryMetadata, ComponentFactoryMetadata } from './componentDecorator';
