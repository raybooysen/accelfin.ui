interface ComponentMetadata {
    componentFactoryKey: string;
    shortName: string;
    isWorkspaceItem: boolean;
}

export default ComponentMetadata;
